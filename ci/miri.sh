#!/usr/bin/env sh

set -ex

export CARGO_NET_RETRY=5
export CARGO_NET_TIMEOUT=10

rustup toolchain install nightly --allow-downgrade -c miri
rustup default nightly

rustup component add miri
cargo miri setup

cargo miri test
